<!---
Author: Daniel Boxhammer
-->

## Data Science | Spezialisierung (D), specialization (E) [^1]
[<img src="/pic/digital_udacity.png" width="200" />](https://www.udacity.com/course/data-scientist-nanodegree--nd025)
[<img src="/pic/digital_heise_da.png" width="200" />](https://stackfuel.heise.de/data-analyst-mit-fokus-python?wt_mc=intern.abo.lll.datenexperte_data_analyst.outbrain.native.native&wt_eid=6161399709019218470&wt_t=1618625661340)
[<img src="/pic/digital_epfl.png" width="200" />](https://www.dsfm.ch/?gclid=Cj0KCQiAtrnuBRDXARIsABiN-7AeEXBVH07p1HQaQyp0kxhlflRaRHyvemFvGVmZqQAXdD_EQdjNhvUaAks2EALw_wcB)
[<img src="/pic/company_barc.png" width="200" />](https://barc.de/)  
[<img src="/pic/digital_sit.png" width="200" />](https://sit.academy/de/data-science)
[<img src="/pic/digital_das-eth-data_scientist.png" width="200" />](https://inf.ethz.ch/continuing-education/das-datascience.html)
[<img src="/pic/digital_cas-nw-data.png" width="200" />](https://www.fhnw.ch/de/weiterbildung/technik/cas-data-science?gclid=CjwKCAiA68ebBhB-EiwALVC-NsFX1aMmcXpZsM5dGST_ctFi_anYFssbvuhA6cbPh3Zq5oxbMd6kjRoCiEUQAvD_BwE)
[<img src="/pic/digital_cas-data-hwz.png" width="200" />](https://fh-hwz.ch/bildungsangebot/weiterbildung/cas-das/cas-applied-data-analytics-hwz?gclid=CjwKCAiA68ebBhB-EiwALVC-Ntv61QbBVTQnvcc6eXgrfHWLsfzNfjrUsZkGrdhnZ6DtsrewTcNJ6BoCkUMQAvD_BwE)


### NANODEGREE PROGRAMS (UdaCity)
* [Data Architect](https://www.udacity.com/course/data-architect-nanodegree--nd038)
* [Data Product Manager](https://www.udacity.com/course/data-product-manager-nanodegree--nd030)
* [SQL](https://www.udacity.com/course/learn-sql--nd072)
* [Data Analyst](https://www.udacity.com/course/data-analyst-nanodegree--nd002)
* [Predictive Analytics for Business Data Scientist](https://www.udacity.com/course/predictive-analytics-for-business-nanodegree--nd008)
* [Data Scientist](https://www.udacity.com/course/data-scientist-nanodegree--nd025)
* [Data Engineer](https://www.udacity.com/course/data-engineer-nanodegree--nd027)
* [Marketing Analytics](https://www.udacity.com/course/marketing-analytics-nanodegree--nd028)
* [Business Analytics](https://www.udacity.com/course/business-analytics-nanodegree--nd098)
* [Programming for Data Science with Python](https://www.udacity.com/course/programming-for-data-science-nanodegree--nd104)
* [Programming for Data Science with R](https://www.udacity.com/course/programming-for-data-science-nanodegree-with-R--nd118)
* [Data Visualization](https://www.udacity.com/course/data-visualization-nanodegree--nd197)
* [Data Structures and Algorithms Data Streaming](https://www.udacity.com/course/data-structures-and-algorithms-nanodegree--nd256)
* [Data Streaming](https://www.udacity.com/course/data-streaming-nanodegree--nd029)
* [Data Science for Business Leaders](https://www.udacity.com/course/data-science-for-business-leaders--nd045)
* [Data Analysis and Visualization with Power BI](https://www.udacity.com/course/data-analysis-and-visualization-with-power-BI-nanodegree--nd331)

[^1]: (Liste nicht vollständig)  
