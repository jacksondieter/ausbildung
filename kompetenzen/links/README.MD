<!---
Author: Daniel Boxhammer
-->


## Anbieter Kompetenzen [^1] 

[<img src="/pic/skils_worklifestyle.png" width="200" />](https://worklifestyle.net/)
[<img src="/pic/skils_16personalities.png" width="200" />](https://www.16personalities.com/de)
[<img src="/pic/skils_pdflist.png" width="200" />](http://www.be-werbung.ch/assets/content/dokumente/faehigkeiten/faehigkeiten_liste_12.pdf)
[<img src="/pic/skils_ipersonic.png" width="200" />](https://www.ipersonic.de/test.html)  

[<img src="/pic/skils_zhlaufbahn.png" width="200" />](http://www.persoenlichkeitsstaerken.ch/)
[<img src="/pic/skils_zhaw.png" width="200" />](https://www.laufbahndiagnostik.ch/fragebogen)
[<img src="/pic/skils_ictberatung.png" width="200" />](https://www.ict-weiterbildung.ch/#contact)  


[^1]: (Liste nicht vollständig)  
